import math as m
import matplotlib.pyplot as plt
import numpy as np

def fatorial(x):
    y = 1;
    while x!=0:
        y = y * x
        x = x - 1
    return y
