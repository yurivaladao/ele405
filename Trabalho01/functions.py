import numpy as np
import math
from numpy.linalg import inv

from pandas import DataFrame, read_csv
import matplotlib.pyplot as plt
import pandas as pd 
import time

##---------------------------------------
##-calculo de aproximação da covariancia-
##---------------------------------------
def covariancia(X,Y,teta,teta_chapeu,phi):
    t = np.linspace(X[0],X[-1],len(X))
    f_t = []
    for i in range(0, len(t)):
        f_t_parcial = 0
        ii = teta - 1
        for j in range(0, len(teta_chapeu)):
            f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
            ii = ii - 1
        f_t.append (f_t_parcial)     
   
    v_teta = 0.0
    for i in range(0, len(X)):
        v_teta = v_teta + ((f_t[i] - Y[i])**2) 
        
    S_2 = 2*v_teta/(len(X)-teta)
    #print(S_2)
    
    covariancia = S_2*inv(phi.T@phi)
    #print(covariancia)
    
    return covariancia,S_2

##-------------------------------------
##----Método dos Mínimos Quadrados-----
##-------------------------------------
def mq(X,Y,teta):
    phi = np.zeros((len(X), teta))

    for i in range(0, len(X)):
        ii=0
        for j in reversed(range(0, teta)):
            phi[i,ii] = (math.pow(X[i],j))
            ii = ii+1
    
#    teta_chapeu = (inv(phi.T@phi)@phi.T)@Y
            
    a = (phi.T@phi)
    b = phi.T@Y
    teta_chapeu = np.linalg.solve(a, b)
#    print(np.linalg.eig(a))
#    print(teta_chapeu)
    return teta_chapeu,phi

##-------------------------------------
##------Função de Mostrar Dados--------
##-------------------------------------
def mostra_curva(X,Y,teta, teta_chapeu,titulo,figura):
    t = np.linspace(X[0],X[-1],len(X))
    f_t = []
    for i in range(0, len(t)):
        f_t_parcial = 0
        ii = teta - 1
        for j in range(0, len(teta_chapeu)):
            f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
            ii = ii - 1
        f_t.append (f_t_parcial)

    plt.figure(figura)
    plt.plot(X,Y,".",t,f_t)
#    plt.title (titulo)
    plt.xlabel("Anos")
    plt.ylabel("População")
    qualquer = "./Figuras/Curva_" + str(figura) + ".eps"
#    qualquer = "./Figuras/Figura_" + str(figura) + "_" + time.strftime("%H%M%S") + ".jpg"
    plt.savefig(qualquer)
    
#    plt.figure(figura*10)
#    plt.scatter(Y,f_t)
#    plt.title (titulo)
#    plt.xlabel("Anos")
#    plt.ylabel("População")
#    qualquer = "./Figuras/Correlacao_" + str(figura) + ".jpg"
#    qualquer = "./Figuras/Figura_" + str(figura) + "_" + time.strftime("%H%M%S") + ".jpg"
#    plt.savefig(qualquer)
    
##-------------------------------------
##------------Fatorial-----------------
##-------------------------------------
def fatorial(x):
    y = 1;
    while x!=0:
        y = y * x
        x = x - 1
    return y
