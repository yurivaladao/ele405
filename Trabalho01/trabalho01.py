import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 

from functions import*

file = r'dados_total.xlsx'
df = pd.read_excel(file)

teta = 7
W = df['Ano']
X = np.array(W)

#Z = df['0-9'] 
#Y = np.array(Z)
#
#[teta_chapeu, phi] = mq(X,Y,teta)
#titulo = "Curva de 0-9 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,1)
#
#Z = df['10-19'] 
#Y = np.array(Z)
#[teta_chapeu, phi] = mq(X,Y,teta)
#titulo = "Curva de 10-19 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,2)
#
#Z = df['20-29'] 
#Y = np.array(Z)
#[teta_chapeu, phi] = mq(X,Y,teta)
#titulo = "Curva de 20-29 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,3)
#
#Z = df['30-39'] 
#Y = np.array(Z)
#[teta_chapeu, phi] = mq(X,Y,teta)
#titulo = "Curva de 30-39 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,4)
#
#Z = df['40-49'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de 40-49 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,5)
#
#Z = df['50-59'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de 50-59 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,6)
#
#Z = df['60-69'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de 60-69 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,7)
#
#Z = df['70-79'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de 70-79 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,8)
#
#Z = df['80-89'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de 80-89 anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,9)
#
#Z = df['90+'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de 90+ anos"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,10)
#
#Z = df['Total'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#titulo = "Curva de Total Idades"
#mostra_curva(X,Y,teta,teta_chapeu,titulo,11)
#[cov, s_2] = covariancia(X,Y,teta,teta_chapeu,phi)
#print(cov)
#
#file = r'dados_mulheres.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#teta_mulher = teta_chapeu
#mostra_curva(X,Y,teta,teta_chapeu,titulo,12)
#[cov, s_2] = covariancia(X,Y,teta,teta_chapeu,phi)
##print(cov)
#
#file = r'dados_homens.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#[teta_chapeu,phi] = mq(X,Y,teta)
#teta_homem = teta_chapeu
#mostra_curva(X,Y,teta,teta_chapeu,titulo,12)
#[cov, s_2] = covariancia(X,Y,teta,teta_chapeu,phi)
#print(cov)

file = r'dados_total.xlsx'
df = pd.read_excel(file)
Z = df['até 40 anos'] 
titulo = "Curva de Comparação"
Y = np.array(Z)
[teta_chapeu,phi] = mq(X,Y,teta)
teta_menor = teta_chapeu
mostra_curva(X,Y,teta,teta_chapeu,titulo,13)

file = r'dados_total.xlsx'
df = pd.read_excel(file)
Z = df['+40'] 
titulo = "Curva de Comparação"
Y = np.array(Z)
[teta_chapeu,phi] = mq(X,Y,teta)
teta_maior = teta_chapeu
mostra_curva(X,Y,teta,teta_chapeu,titulo,13)

#-------------------------------------------
#------------ Gráfico de Regiões -----------
#-------------------------------------------

#plt.figure(21)
#
#file = r'dados_norte.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#plt.plot(X,Y,label='Norte')
#
#file = r'dados_nordeste.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#plt.plot(X,Y,label='Nordeste')
#
#file = r'dados_sul.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#plt.plot(X,Y,label='Sul')
#
#file = r'dados_sudeste.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#plt.plot(X,Y,label='Sudeste')
#
#file = r'dados_centro.xlsx'
#df = pd.read_excel(file)
#Z = df['Total'] 
#Y = np.array(Z)
#plt.plot(X,Y,label='Centro-Oeste')
#plt.legend()
#plt.xlabel("Anos")
#plt.ylabel("População")
#qualquer = "./Figuras/Regioes.eps"
#plt.savefig(qualquer)



#-------------------------------------------
#------------- Gráfico de Erros ------------
#-------------------------------------------


#W = df['Ano']
#X = np.array(W)
#
#Z = df['0-9'] 
#Y = np.array(Z)
#
#plt.figure(45)
#plt.plot(X,Y,".", label='Dados Originais')
#
#teta = 3
#[teta_chapeu, phi] = mq(X,Y,teta)
#t = np.linspace(X[0],X[-1],len(X))
#
#f_t = []
#for i in range(0, len(t)):
#    f_t_parcial = 0
#    ii = teta - 1
#    for j in range(0, len(teta_chapeu)):
#        f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
#        ii = ii - 1
#    f_t.append (f_t_parcial)
#
#plt.plot(X,f_t, label='3 parâmetros')
#
#teta = 4
#[teta_chapeu, phi] = mq(X,Y,teta)
#t = np.linspace(X[0],X[-1],len(X))
#
#f_t = []
#for i in range(0, len(t)):
#    f_t_parcial = 0
#    ii = teta - 1
#    for j in range(0, len(teta_chapeu)):
#        f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
#        ii = ii - 1
#    f_t.append (f_t_parcial)
#
#plt.plot(X,f_t, label='4 parâmetros')
#
#teta = 5
#[teta_chapeu, phi] = mq(X,Y,teta)
#t = np.linspace(X[0],X[-1],len(X))
#
#f_t = []
#for i in range(0, len(t)):
#    f_t_parcial = 0
#    ii = teta - 1
#    for j in range(0, len(teta_chapeu)):
#        f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
#        ii = ii - 1
#    f_t.append (f_t_parcial)
#
#plt.plot(X,f_t, label='5 parâmetros')
#
#teta = 6
#[teta_chapeu, phi] = mq(X,Y,teta)
#t = np.linspace(X[0],X[-1],len(X))
#
#f_t = []
#for i in range(0, len(t)):
#    f_t_parcial = 0
#    ii = teta - 1
#    for j in range(0, len(teta_chapeu)):
#        f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
#        ii = ii - 1
#    f_t.append (f_t_parcial)
#
#plt.plot(X,f_t, label='6 parâmetros')
#
#teta = 10
#[teta_chapeu, phi] = mq(X,Y,teta)
#t = np.linspace(X[0],X[-1],len(X))
#
#f_t = []
#for i in range(0, len(t)):
#    f_t_parcial = 0
#    ii = teta - 1
#    for j in range(0, len(teta_chapeu)):
#        f_t_parcial = f_t_parcial + ((math.pow(t[i],ii))*teta_chapeu[j])
#        ii = ii - 1
#    f_t.append (f_t_parcial)
#
#plt.plot(X,f_t, label='10 parâmetros')
#plt.legend()
#plt.xlabel("Anos")
#plt.ylabel("População")
#qualquer = "./Figuras/Erros.jpg"
#plt.savefig(qualquer)
    

