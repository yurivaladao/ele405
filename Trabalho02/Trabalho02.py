"""
Created on Thu Apr 25 10:58:25 2019

@author: Usuário
"""

import numpy as np
from numpy.linalg import inv
import scipy
from scipy import signal

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#from functions02 import*

plt.close("all")

define_figura = 1 # teste para gerar imagens ou não // 0 - Não; 1 - Sim

M = 1000 # numero de tentativas de melhoria
N = 2000 # numero de dados

u_degrau = np.ones(N)*5 # degrau
t = np.arange(N)

u_sinu = np.sin(t/50) + np.cos(t/20)

h = 10**-4 #periodo de amostragem do sinal

# Componentes RLC
L = 100*10**-3 
R = 10
C = 20*10**(-6)

# função de transferencia
B = [h**2]
A = [(L*C), (h*R*C - 2*L*C), (L*C - h*R*C + h**2)]

y_d = np.zeros(N)
y_d = signal.lfilter(B,A, u_degrau)

y_s = np.zeros(N)
y_s = signal.lfilter(B,A, u_sinu)

t = np.arange(N)

if (define_figura == 1):
    plt.figure()
    plt.plot(t,y_d,'k')
    plt.xlabel("k")
    plt.ylabel("y")
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau.eps"
    plt.savefig(qualquer)

    plt.figure()
    plt.plot(t,y_s,'k')
    plt.xlabel("k")
    plt.ylabel("y")
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu.eps"
    plt.savefig(qualquer)

theta_total = np.zeros((M,3))
for tentativas in range(0,M):
    r = np.random.randn(N)/N #gera vetor ruido    
    v = signal.lfilter([1],A,r) #v é o ruido filtrado
    yr = y_d + v

    phi = np.zeros((N, 3))
        
    for i in range(2,N):
        phi[i,0] = u_degrau[i-1]
        phi[i,1] = -yr[i-1]
        phi[i,2] = -yr[i-2]
        
    theta_chapeu = (inv(phi.T@phi)@phi.T)@yr
    
    num = np.array([1,theta_chapeu[1],theta_chapeu[2]]) 
    
    # Predição do sinal
    y_pred = signal.lfilter([theta_chapeu[0]], [1], u_degrau) - signal.lfilter(num, [1], yr) + yr

    theta_total[tentativas,0] = theta_chapeu[0]
    theta_total[tentativas,1] = theta_chapeu[1]
    theta_total[tentativas,2] = theta_chapeu[2]
    
    soma_erro = 0.0
    for cont in range(0,N):
         soma_erro = (yr[cont] - y_pred[cont])**2
    erro_temp = (soma_erro/N)**(1/2)
    
    if(tentativas==0):
        erro_calc = erro_temp
        y_melhor = y_pred
        yr_melhor = yr
        theta_melhor = theta_chapeu
    else:
        if(erro_temp<erro_calc): #testa se o erro é menor
            erro_calc = erro_temp #atualiza o erro
            y_melhor = y_pred
            yr_melhor = yr
            theta_melhor = theta_chapeu

# Simulação do sinal
num = np.array([1, theta_melhor[0]])
den = np.array([1, theta_melhor[1],theta_melhor[2]])

y_simulado = signal.lfilter(num,den,u_degrau)


if (define_figura == 1):
    plt.figure()
    plt.plot(t,y_d,'k',label='Original')
    plt.plot(t,y_simulado,'r',label='Simulado')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_Simulado.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_Simulado.eps"
    plt.savefig(qualquer)
    
    plt.figure()
    plt.plot(t,yr_melhor,'k',label='Ruido')
    plt.plot(t,y_melhor,'r',label='Preditivo')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_Ruido.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_Ruido.eps"
    plt.savefig(qualquer)

    theta_1 = np.zeros((M, 1))
    theta_2 = np.zeros((M, 1))
    theta_3 = np.zeros((M, 1))
    
    for j in range(0,M):
        theta_1[j] = theta_total[j,0]
        theta_2[j] = theta_total[j,1]
        theta_3[j] = theta_total[j,2]
        
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(theta_1,theta_2,theta_3)
    ax.set_xlabel('Theta 1')
    ax.set_ylabel('Theta 2')
    ax.set_zlabel('Theta 3')
    plt.show()
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_Theta.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_Theta.eps"
    plt.savefig(qualquer)
    
    
    
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
    
theta_total = np.zeros((M,3))
for tentativas in range(0,M):
    r = np.random.randn(N)/N #gera vetor ruido    
    yr = y_d + r

    phi = np.zeros((N, 3))
        
    for i in range(2,N):
        phi[i,0] = u_degrau[i-1]
        phi[i,1] = -yr[i-1]
        phi[i,2] = -yr[i-2]
        
    theta_chapeu = (inv(phi.T@phi)@phi.T)@yr
    
    num = np.array([1,theta_chapeu[1],theta_chapeu[2]]) 
    
    # Predição do sinal
    y_pred = signal.lfilter([theta_chapeu[0]], [1], u_degrau) - signal.lfilter(num, [1], yr) + yr

    theta_total[tentativas,0] = theta_chapeu[0]
    theta_total[tentativas,1] = theta_chapeu[1]
    theta_total[tentativas,2] = theta_chapeu[2]
    
    soma_erro = 0.0
    for cont in range(0,N):
         soma_erro = (yr[cont] - y_pred[cont])**2
    erro_temp = (soma_erro/N)**(1/2)
    
    if(tentativas==0):
        erro_calc = erro_temp
        y_melhor = y_pred
        yr_melhor = yr
        theta_melhor = theta_chapeu
    else:
        if(erro_temp<erro_calc): #testa se o erro é menor
            erro_calc = erro_temp #atualiza o erro
            y_melhor = y_pred
            yr_melhor = yr
            theta_melhor = theta_chapeu

# Simulação do sinal
num = np.array([1, theta_melhor[0]])
den = np.array([1, theta_melhor[1],theta_melhor[2]])

y_simulado = signal.lfilter(num,den,u_degrau)


if (define_figura == 1):
    plt.figure()
    plt.plot(t,y_d,'k',label='Original')
    plt.plot(t,y_simulado,'r',label='Simulado')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_S_Simulado.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_S_Simulado.eps"
    plt.savefig(qualquer)
    
    plt.figure()
    plt.plot(t,yr_melhor,'k',label='Ruido')
    plt.plot(t,y_melhor,'r',label='Preditivo')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_S_Ruido.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_S_Ruido.eps"
    plt.savefig(qualquer)

    theta_1 = np.zeros((M, 1))
    theta_2 = np.zeros((M, 1))
    theta_3 = np.zeros((M, 1))
    
    for j in range(0,M):
        theta_1[j] = theta_total[j,0]
        theta_2[j] = theta_total[j,1]
        theta_3[j] = theta_total[j,2]
        
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(theta_1,theta_2,theta_3)
    ax.set_xlabel('Theta 1')
    ax.set_ylabel('Theta 2')
    ax.set_zlabel('Theta 3')
    plt.show()
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_S_Theta.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Degrau_S_Theta.eps"
    plt.savefig(qualquer)
    
    
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
#       Sinusoidal
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
    
    
theta_total = np.zeros((M,3))
for tentativas in range(0,M):
    r = np.random.randn(N)/N #gera vetor ruido    
    v = signal.lfilter([1],A,r) #v é o ruido filtrado
    yr = y_s + v

    phi = np.zeros((N, 3))
        
    for i in range(2,N):
        phi[i,0] = u_sinu[i-1]
        phi[i,1] = -yr[i-1]
        phi[i,2] = -yr[i-2]
        
    theta_chapeu = (inv(phi.T@phi)@phi.T)@yr
    
    num = np.array([1,theta_chapeu[1],theta_chapeu[2]]) 
    
    # Predição do sinal
    y_pred = signal.lfilter([theta_chapeu[0]], [1], u_sinu) - signal.lfilter(num, [1], yr) + yr

    theta_total[tentativas,0] = theta_chapeu[0]
    theta_total[tentativas,1] = theta_chapeu[1]
    theta_total[tentativas,2] = theta_chapeu[2]
    
    soma_erro = 0.0
    for cont in range(0,N):
         soma_erro = (yr[cont] - y_pred[cont])**2
    erro_temp = (soma_erro/N)**(1/2)
    
    if(tentativas==0):
        erro_calc = erro_temp
        y_melhor = y_pred
        yr_melhor = yr
        theta_melhor = theta_chapeu
    else:
        if(erro_temp<erro_calc): #testa se o erro é menor
            erro_calc = erro_temp #atualiza o erro
            y_melhor = y_pred
            yr_melhor = yr
            theta_melhor = theta_chapeu

# Simulação do sinal
num = np.array([1, theta_melhor[0]])
den = np.array([1, theta_melhor[1],theta_melhor[2]])

y_simulado = signal.lfilter(num,den,u_sinu)


if (define_figura == 1):
    plt.figure()
    plt.plot(t,y_s,'k',label='Original')
    plt.plot(t,y_simulado,'r',label='Simulado')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_Simulado.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_Simulado.eps"
    plt.savefig(qualquer)
    
    plt.figure()
    plt.plot(t,yr_melhor,'k',label='Ruido')
    plt.plot(t,y_melhor,'r',label='Preditivo')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_Ruido.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_Ruido.eps"
    plt.savefig(qualquer)

    theta_1 = np.zeros((M, 1))
    theta_2 = np.zeros((M, 1))
    theta_3 = np.zeros((M, 1))
    
    for j in range(0,M):
        theta_1[j] = theta_total[j,0]
        theta_2[j] = theta_total[j,1]
        theta_3[j] = theta_total[j,2]
        
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(theta_1,theta_2,theta_3)
    ax.set_xlabel('Theta 1')
    ax.set_ylabel('Theta 2')
    ax.set_zlabel('Theta 3')
    plt.show()
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_Theta.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_Theta.eps"
    plt.savefig(qualquer)
    
    
    
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
#--------------------------------------------------------------
    
theta_total = np.zeros((M,3))
for tentativas in range(0,M):
    r = np.random.randn(N)/N #gera vetor ruido    
    yr = y_s + r

    phi = np.zeros((N, 3))
        
    for i in range(2,N):
        phi[i,0] = u_sinu[i-1]
        phi[i,1] = -yr[i-1]
        phi[i,2] = -yr[i-2]
        
    theta_chapeu = (inv(phi.T@phi)@phi.T)@yr
    
    num = np.array([1,theta_chapeu[1],theta_chapeu[2]]) 
    
    # Predição do sinal
    y_pred = signal.lfilter([theta_chapeu[0]], [1], u_sinu) - signal.lfilter(num, [1], yr) + yr

    theta_total[tentativas,0] = theta_chapeu[0]
    theta_total[tentativas,1] = theta_chapeu[1]
    theta_total[tentativas,2] = theta_chapeu[2]
    
    soma_erro = 0.0
    for cont in range(0,N):
         soma_erro = (yr[cont] - y_pred[cont])**2
    erro_temp = (soma_erro/N)**(1/2)
    
    if(tentativas==0):
        erro_calc = erro_temp
        y_melhor = y_pred
        yr_melhor = yr
        theta_melhor = theta_chapeu
    else:
        if(erro_temp<erro_calc): #testa se o erro é menor
            erro_calc = erro_temp #atualiza o erro
            y_melhor = y_pred
            yr_melhor = yr
            theta_melhor = theta_chapeu

# Simulação do sinal
num = np.array([1, theta_melhor[0]])
den = np.array([1, theta_melhor[1],theta_melhor[2]])

y_simulado = signal.lfilter(num,den,u_sinu)


if (define_figura == 1):
    plt.figure()
    plt.plot(t,y_s,'k',label='Original')
    plt.plot(t,y_simulado,'r',label='Simulado')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_S_Simulado.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_S_Simulado.eps"
    plt.savefig(qualquer)
    
    plt.figure()
    plt.plot(t,yr_melhor,'k',label='Ruido')
    plt.plot(t,y_melhor,'r',label='Preditivo')
    plt.xlabel("k")
    plt.ylabel("y")
    plt.legend()
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_S_Ruido.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_S_Ruido.eps"
    plt.savefig(qualquer)

    theta_1 = np.zeros((M, 1))
    theta_2 = np.zeros((M, 1))
    theta_3 = np.zeros((M, 1))
    
    for j in range(0,M):
        theta_1[j] = theta_total[j,0]
        theta_2[j] = theta_total[j,1]
        theta_3[j] = theta_total[j,2]
        
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(theta_1,theta_2,theta_3)
    ax.set_xlabel('Theta 1')
    ax.set_ylabel('Theta 2')
    ax.set_zlabel('Theta 3')
    plt.show()
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_S_Theta.jpg"
    plt.savefig(qualquer)
    qualquer = "./Figuras/Trabalho_02/Resposta_Sinu_S_Theta.eps"
    plt.savefig(qualquer)